# README #

This repository is a collection of configurations needed to register Keboola Generic Extractor as a branded Asana KBC Extractor.
Extractor's task is to help user to extract the data from Asana to Keboola Connection Platform (KBC). 

## API documentation ##
[Asana API documentation](https://asana.com/guide/help/api/api)  

## Configuration ##

Required Information:  
   
  1. API Key  
    - API key is mandatory for the extractor to gain access to Asana 
    - To create API Token:
        1. Login to Asana
        2. Click on your name icon at the top right corner of the browser
        3. Click My Profile Settings...
        4. Click on Apps
        5. Managed Developer Apps
        6. Create New Personal Access Token


## Contact Info ##
Leo Chan  
Vancouver, Canada (PST time)   
Email: leo@keboola.com  
Private: cleojanten@hotmail.com   